import { browser, by, element,  protractor} from 'protractor';

var EC = protractor.ExpectedConditions;

var  Homepagelogin = function() {
  browser.get('https://release-frontend-as.azurewebsites.net/#/auth/login');
  browser.ignoreSynchronization = true;
  browser.wait(EC.elementToBeClickable(element(by.id("login-form-button"))), 5000);
  element(by.name("company")).sendKeys("Yellax Front-end Testing");
  element(by.name("username")).sendKeys("Bryan");
  element(by.name("password")).sendKeys("Wachtwoord.2");
  browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 900000); 
  element(by.id("login-form-button")).click();
  browser.wait(EC.urlContains('home'), 20000);
  browser.wait(EC.textToBePresentInElement(element(by.className("home-top-subtitle")),'Beheer hier uw artikelen'), 10000);
}

var loginout = function() {
  browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 
  browser.wait(EC.elementToBeClickable(element(by.className("nav-bar-user-arrow fas fa-angle-down"))), 5000);
  element(by.className("nav-bar-user-arrow fas fa-angle-down")).click();
  browser.wait(EC.elementToBeClickable(element(by.xpath("//span[@class='nav-bar-user-menu-element-text'][text()='Uitloggen']"))), 8000);
  browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
  element(by.xpath("//span[@class='nav-bar-user-menu-element-text'][text()='Uitloggen']")).click();
  browser.wait(EC.urlContains('login'), 20000);
}

describe('generated description', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
   

  it('create manufacturer by generated description',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.actions().doubleClick(element(by.id("grid-page-top-bar-search-input"))).sendKeys("installatieautomaat").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(3000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']"))), 5000);
    element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']")).click();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.wait(EC.visibilityOf(element.all(by.className("list-table")).get(5)), 5000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Item toevoegen']"))), 5000 );
    element(by.xpath("//button[text()='Item toevoegen']")).click();
    var inputbox = element.all(by.className("dx-texteditor-input"));
    browser.actions().click(inputbox.get(1)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Artikel - Fabrikant naam']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(2)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Waarde']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(3)).sendKeys("Frabrikant: ").perform();
    browser.actions().click(inputbox.get(4)).sendKeys("_").perform();
    browser.actions().click(inputbox.get(5)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Altijd zichtbaar']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']"))), 5000);
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']")).click();
    browser.sleep(1000);
    browser.actions().mouseMove(element.all(by.className("dx-list-reorder-handle")).get(6)).perform();
    browser.actions().mouseDown().perform();
    browser.actions().mouseMove({x: 0, y: -252}).perform();
    browser.actions().mouseUp().perform();
    browser.sleep(1000);
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );

    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });

  it('look at the generated ',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    element(by.className("nav-bar-search-box")).clear();
    browser.actions().doubleClick(element(by.className("nav-bar-search-box"))).sendKeys("installatieautomaat").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[5]/td")),'Y'), 50000);
    browser.actions().click(element(by.className("nav-bar-search-box"))).sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[5]/td")),'5SY66167'), 50000);
    expect(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[3]/td")).getText()).toContain("Frabrikant: Siemens Industry_");
  });

  it('delete the manufacturer',function() {
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );

    browser.actions().click(element(by.id("grid-page-top-bar-search-input"))).sendKeys("installatieautomaat").sendKeys(protractor.Key.ENTER).perform();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );


    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']"))), 5000);
    element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']")).click();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
  
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Item toevoegen']"))), 11000 );
    
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[@class='description-builder-property'][text()='Artikel - Fabrikant naam']")),'Artikel - Fabrikant naam'), 8000 );

    var deleteProperty = element.all(by.xpath("//em[@class='fal fa-trash-alt']"));
    deleteProperty.get(0).click();

    browser.sleep(1000);
    
    browser.wait(EC.visibilityOf(element(by.xpath("//button[text()='Doorgaan']"))), 8000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Doorgaan']"))), 5000 );
    element(by.xpath("//button[text()='Doorgaan']")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.sleep(1500); // moet blijven
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    browser.sleep(1500); // moet blijven
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    element(by.id("notification-bell")).click();
    browser.sleep(1500); // moet blijven
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });
});
