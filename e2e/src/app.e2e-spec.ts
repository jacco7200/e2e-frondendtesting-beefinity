import { browser, by, element,  protractor} from 'protractor';

var EC = protractor.ExpectedConditions;

/* Preconditions: 
- Gegenereerde omschrijving van MCB != fab. naam && size() == 5;
- ashdfjkasdhfkjasdhfjkh



*/

var  Homepagelogin = function() {
  browser.get('https://release-frontend-as.azurewebsites.net/#/auth/login');
  browser.ignoreSynchronization = true;
  browser.wait(EC.elementToBeClickable(element(by.id("login-form-button"))), 5000);
  element(by.name("company")).sendKeys("Yellax Front-end Testing");
  element(by.name("username")).sendKeys("Bryan");
  element(by.name("password")).sendKeys("Wachtwoord.2");
  browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 900000); 
  element(by.id("login-form-button")).click();
  browser.wait(EC.urlContains('home'), 200000);
  browser.wait(EC.textToBePresentInElement(element(by.className("home-top-subtitle")),'Beheer hier uw artikelen'), 100000);
}

var loginout = function() {
  browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 
  browser.wait(EC.elementToBeClickable(element(by.className("nav-bar-user-arrow fas fa-angle-down"))), 5000);
  element(by.className("nav-bar-user-arrow fas fa-angle-down")).click();
  browser.wait(EC.elementToBeClickable(element(by.xpath("//span[@class='nav-bar-user-menu-element-text'][text()='Uitloggen']"))), 8000);
  browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
  element(by.xpath("//span[@class='nav-bar-user-menu-element-text'][text()='Uitloggen']")).click();
  browser.wait(EC.urlContains('login'), 20000);
}

describe('Online search without loggin in', function() {
  it('Should show a search bar',function() {
  browser.get('https://release-frontend-as.azurewebsites.net/#/online/search');
  browser.ignoreSynchronization = true;
  browser.wait(EC.elementToBeClickable(element(by.buttonText("Search"))), 5000);
  });
  it('Should search for provided GTIN',function() {
  element(by.className('online-search-input-box')).sendKeys("4001869321103");
  element(by.buttonText('Search')).click();
  browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
  browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='4001869321103']")),'4001869321103'), 60000);
  element(by.className("article-image-grid-button")).click();
  browser.wait(EC.textToBePresentInElement(element(by.xpath("//div[text()='4001869321103']")),'4001869321103'), 60000);
  expect(element(by.xpath("//*[text()='5SJ42067HG41']")).isPresent()).toBe(true);
  expect(element(by.className("nav-bar-button-register")).isPresent()).toBe(true);
  expect(element(by.className("nav-bar-button-login pointer")).isPresent()).toBe(true);
  });
});

describe('Beefinity inlog page', function() {
  beforeEach(function() {
    browser.get('https://release-frontend-as.azurewebsites.net/#/auth/login');
    browser.ignoreSynchronization = true;
  });

  it('Forgot your password?',function() {
    browser.wait(EC.elementToBeClickable(element(by.xpath("//a[text()=' Forgot password?']"))), 10000);
    element(by.xpath("//a[text()=' Forgot password?']")).click();
    expect(browser.getCurrentUrl()).toEqual("https://release-frontend-as.azurewebsites.net/#/auth/password-reset/undefined");
  });
  it('Create a free account',function() {
    browser.wait(EC.elementToBeClickable(element(by.xpath("//a[text()=' Register for free']"))), 12000);
    element(by.xpath("//a[text()=' Register for free']")).click();
    expect(browser.getCurrentUrl()).toEqual("https://release-frontend-as.azurewebsites.net/#/auth/register");
  });
  it('get login error',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("login-form-button"))), 10000);
    element(by.name("company")).sendKeys("yellax");
    element(by.name("username")).sendKeys("test");
    element(by.name("password")).sendKeys("Wachtwoord.1");

    element(by.id("login-form-button")).click();
    browser.wait(EC.textToBePresentInElement(element(by.id("login-form-error")), 'Username and/or password is invalid!'), 5000);
  });
  it('login successful',function() {
    browser.refresh();
    browser.wait(EC.elementToBeClickable(element(by.id("login-form-button"))), 5000);
    element(by.name("company")).sendKeys("Yellax Front-end Testing");
    element(by.name("username")).sendKeys("Bryan");
    element(by.name("password")).sendKeys("Wachtwoord.2");
    element(by.id("login-form-button")).click();
    // browser.wait(EC.elementToBeClickable(element(by.xpath("//*[text()='Proceed']"))), 30000);
    // element(by.xpath("//*[text()='Proceed']")).click();
    browser.wait(EC.urlContains('home'), 500000);
  });
});

describe('Home page search', function() {
  beforeEach(function() {
    Homepagelogin();
  });

  afterEach(function(){
    loginout();
  });
   
  var searchbox = element.all(by.className("home-results-column-search-box"));
  var searchlabel = element.all(by.className("home-results-column-selection-label"));
  var searchEnter = element.all(by.className("home-results-column-search-icon fal fa-search"))

  it('search by manufactures',function() {
    browser.wait(EC.elementToBeClickable(searchlabel.get(0)), 6000);
    browser.actions().click(searchbox.get(0)).sendKeys("ABB").perform();   
    browser.actions().click(searchEnter.get(0)).perform();
    browser.actions().click(searchlabel.get(1)).perform(); 
    browser.wait(EC.textToBePresentInElement(element(by.className("selected-filters-name")),'ABB Componenten'), 10000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']"))), 6000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
  });

  it('search by groups',function() {
    browser.wait(EC.elementToBeClickable(searchlabel.get(1)), 6000);
    browser.actions().click(searchbox.get(1)).sendKeys("installatieautomaat").perform();   
    browser.actions().click(searchEnter.get(1)).perform();
    browser.actions().click(searchlabel.get(20)).perform(); 
    browser.wait(EC.textToBePresentInElement(element(by.className("selected-filters-name")),'Installatieautomaat'), 10000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: A9F75301']")),'A9F75301'), 50000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']"))), 5000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 10000);
  });

  it('search in online',function() {
    browser.wait(EC.elementToBeClickable(searchlabel.get(2)), 6000);
    browser.actions().click(searchbox.get(2)).sendKeys("5SY4").perform();   
    browser.actions().click(searchEnter.get(2)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='4001869181370']")),'4001869181370'), 5000);
  });
});

describe('filter on the article tab', function() {
  beforeEach(function() {
    Homepagelogin();
  });

  afterEach(function() {
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']"))), 20000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    loginout();
  });
 
  it('filteren',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    
    var searchbox = element.all(by.className("filter-element-filter-search-box"));
    var searchEnter = element.all(by.className("filter-element-filter-search-icon fal fa-search"));
    var searchlabel = element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label"));
    browser.sleep(3000);
    browser.actions().click(searchbox.get(0)).sendKeys("installatieautomaat").perform();   
    browser.actions().click(searchEnter.get(0)).perform();
    browser.actions().click(element(by.xpath("//*[@class='filter-element-selection-label']"))).perform(); 
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(1500); // moet blijven
    browser.actions().click(searchbox.get(1)).sendKeys("Schneider Electric").perform();   
    browser.actions().click(searchEnter.get(1)).perform();
    browser.actions().click(searchlabel.get(0)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(1),'Domae'), 50000);
      
    browser.actions().click(searchlabel.get(13)).perform();
  
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    
    browser.actions().mouseDown(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div[1]/div[1]/articles-filters/div/div[2]/div[3]/articles-filters-features/div/div[2]/div/div[2]/ng5-slider/span[6]"))).perform();
    browser.actions().mouseMove({x: -42, y: 0}).perform();
    browser.actions().mouseUp().perform();

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 250000 );

    browser.actions().mouseDown(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div[1]/div[1]/articles-filters/div/div[2]/div[3]/articles-filters-features/div/div[2]/div/div[2]/ng5-slider/span[5]"))).perform();
    browser.actions().mouseMove({x: 64, y: 0}).perform();
    browser.actions().mouseUp().perform();

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );

    var filter = element.all(by.className("selected-filters-name"));

    expect(filter.count()).toBe(4); 
    expect(filter.get(0).getText()).toBe('Installatieautomaat');
    expect(filter.get(1).getText()).toBe('Schneider Electric');
    expect(filter.get(2).getText()).toBe('C');
    expect(filter.get(3).getText()).toBe('20...50 A');
    expect(element(by.xpath("//*[text()='Bestelnummer: 19200']")).getText()).toContain('19200');
  });
});

describe('copare articles', function() {
  beforeEach(function() {
    Homepagelogin();
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
  });
  afterEach(function(){
    loginout();
  });
   
  it('copare articles',function() {
     
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000); 
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);

    var artikle = element.all(by.xpath("//div[@class='artilce-info-row']"));
    var coparecheckbox = element.all(by.xpath("//em[@class='compare-icon fal fa-square']"));
    browser.actions().mouseMove(artikle.get(1)).perform();
    coparecheckbox.get(1).click();
    browser.actions().mouseMove(artikle.get(2)).perform();
    coparecheckbox.get(1).click();
    browser.actions().mouseMove(artikle.get(3)).perform();
    coparecheckbox.get(1).click();

    browser.wait(EC.elementToBeClickable(element(by.id("article-compare-small-container"))), 5000);
    element(by.id("article-compare-small-container")).click();
	  browser.wait(EC.elementToBeClickable(element(by.xpath("//*[text()='Vergelijk']"))), 5000);
    element(by.xpath("//*[text()='Vergelijk']")).click();
    browser.wait(EC.urlContains('comparearticles'), 5000);
    browser.wait(EC.elementToBeClickable(element(by.className("show-difference-text"))), 5000);
    element(by.className("show-difference-text")).click();
    
    var vergelijkArtikelen = element.all(by.className("compare-articles-title "));
    expect(vergelijkArtikelen.count()).toEqual(3);
    var removeArtikelen = element.all(by.className("fal fa-times compare-articles-remove-item-icon"));
    browser.actions().click(removeArtikelen.get(1)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000); 
    var vergelijkArtikelen = element.all(by.className("compare-articles-title "));
    expect(vergelijkArtikelen.count()).toEqual(2);
    browser.actions().click(removeArtikelen.get(0)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000); 
    var vergelijkArtikelen = element.all(by.className("compare-articles-title "));
    expect(vergelijkArtikelen.count()).toEqual(1);
  });
});

describe('management test manufactures', function() {
  beforeEach(function() {
    Homepagelogin();
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
  });
  afterEach(function(){
    loginout();
  });
   

  it('Make a new manufactures',function() {
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-plus"))),5000);
    element(by.className("fal fa-plus")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Serie toevoegen"))), 5000);
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().click(inputbox.get(0)).sendKeys("YEL").perform();
    browser.actions().click(inputbox.get(1)).sendKeys("Yellax").perform();
    element(by.buttonText("Serie toevoegen")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Toevoegen"))), 5000);
    browser.actions().click(inputbox.get(2)).sendKeys("BF").perform();
    element(by.buttonText("Toevoegen")).click();
    element(by.buttonText("Serie toevoegen")).click();
    browser.actions().click(inputbox.get(2)).sendKeys("TM").perform();
    element(by.buttonText("Toevoegen")).click();
    var serieicon = element.all(by.className("serie-icons"));
    expect(serieicon.count()).toEqual(2);
    expect(serieicon.get(0).getText()).toBe('BF');
    expect(serieicon.get(1).getText()).toBe('TM');
    browser.sleep(1000);
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
  });

  it('search fot the new manufactures',function() {
    browser.actions().click(element(by.id("grid-page-top-bar-search-input"))).sendKeys("yellax").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//td[text()='YEL']")),'YEL'), 30000);
    expect(element(by.xpath("//td[text()='YEL']")).getText()).toBe('YEL');
    expect(element(by.xpath("//td[text()='Yellax']")).getText()).toBe('Yellax');
    expect(element(by.xpath("//td[text()='BF, TM']")).getText()).toBe('BF, TM');
  });

  it('delete the manufactures',function() {
    browser.actions().click(element(by.id("grid-page-top-bar-search-input"))).sendKeys("yellax").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//td[text()='YEL']")),'YEL'), 30000);
    element(by.className("dx-row dx-data-row dx-column-lines")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash"))), 50000);
    element(by.className("fal fa-trash")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Doorgaan"))), 5000);
    element(by.buttonText("Doorgaan")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
    expect(element(by.className("dx-datagrid-nodata")).getText()).toBe('No data');
  });
});

describe('management test groeps', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
   
  it('Make a new Synoniem and characteristics by "installatieautomaat"',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.sleep(4000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.actions().doubleClick(element(by.id("grid-page-top-bar-search-input"))).sendKeys("installatieautomaat").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.sleep(2000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    var inputbox = element.all(by.className("dx-texteditor-input"))
    browser.actions().click(inputbox.get(3)).sendKeys("Miniature circuit breaker").perform();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[@class='button'][text()='Toevoegen']"))), 5000 );
    element(by.xpath("//button[@class='button'][text()='Toevoegen']")).click();
    expect(element(by.xpath("//div[text()=' Miniature circuit breaker ']")).getText()).toContain('Miniature circuit breaker');
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Kenmerk toevoegen"))), 5000 );
    element(by.buttonText("Kenmerk toevoegen")).click();
    browser.sleep(5000);
    browser.actions().click(inputbox.get(4)).sendKeys("Kleur [Alfanumeriek]").perform();
    browser.sleep(2000);
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Kleur [Alfanumeriek]']")).click();
    browser.sleep(4500);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[@class='modal-bottom-button'][text()='Toevoegen ']"))), 5000 );
    element(by.xpath("//button[@class='modal-bottom-button'][text()='Toevoegen ']")).click();
    browser.sleep(1000);
    element.all(by.className("modal-close fal fa-times")).get(0).click();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/group-detail/div/dx-validation-group/div[1]/group-detail-general/div/div/div[4]/div[2]/div[2]/dx-list/div[1]/div/div[1]/div[2]/div[27]/div[1]/div/div[1]/em[3]"))), 5000 );
    element(by.xpath("/html/body/app-root/authorized-layout/div[4]/group-detail/div/dx-validation-group/div[1]/group-detail-general/div/div/div[4]/div[2]/div[2]/dx-list/div[1]/div/div[1]/div[2]/div[27]/div[1]/div/div[1]/em[3]")).click();
    
    browser.wait(EC.visibilityOf(element(by.buttonText("Koppel"))), 50000);


    browser.actions().click(inputbox.get(4)).sendKeys("EV000235").perform();
    browser.sleep(1000);
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    element(by.buttonText("Koppel")).click();
    browser.actions().click(inputbox.get(4)).sendKeys("EV000080").perform();
    browser.sleep(1000);
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    element(by.buttonText("Koppel")).click();
    element(by.buttonText("Sluiten")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );

    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });

  it('watch the new characteristics ',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.actions().doubleClick(element(by.className("nav-bar-search-box"))).sendKeys("Miniature circuit breaker").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 900000 );
    
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[5]/td")),'Y'), 50000);
    var artikle = element.all(by.xpath("//tbody[@class='dx-template-wrapper']"));
    browser.actions().doubleClick(artikle.get(0)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    var tabheader = element.all(by.className("page-tab-header-item"));
    browser.wait(EC.elementToBeClickable(tabheader.get(2)),5000);
    browser.actions().click(tabheader.get(2)).perform();
    browser.sleep(2000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.wait(EC.invisibilityOf(element(by.xpath("//*[@class='tab-content-detail-field-text'][text()='Kleur']"))), 11000);
    expect(element(by.xpath("//*[@class='tab-content-detail-field-text'][text()='Kleur']")).isPresent).toBeTruthy();
    browser.sleep(2000);
    var inputbox = element.all(by.className("dx-texteditor-input"));
    browser.actions().click(inputbox.get(24)).perform();
    var dropdownlist = element.all(by.className("dx-item-content dx-list-item-content"));
    expect(dropdownlist.count()).toEqual(2);
    expect(dropdownlist.get(0).getText()).toBe('Blauw');
    expect(dropdownlist.get(1).getText()).toBe('Groen');
    browser.actions().click(dropdownlist.get(0)).perform();
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
  });

  it('delete the characteristics error message',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.actions().doubleClick(element(by.id("grid-page-top-bar-search-input"))).sendKeys("installatieautomaat").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    var deleteProperty = element.all(by.xpath("//em[@class='fal fa-trash-alt']"));
    deleteProperty.get(0).click();
    browser.sleep(1000);
    var closebutton = element.all(by.className("modal-close fal fa-times")).get(5);
    browser.wait(EC.elementToBeClickable(closebutton),5000);
    expect(element(by.xpath("//*[@class='modal-title'][text()='Let op!']")).isPresent()).toBeTruthy();
    closebutton.click();
    browser.sleep(1000);
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );

    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });
  
  it('delete the characteristics in the article',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.actions().doubleClick(element(by.className("nav-bar-search-box"))).sendKeys("Miniature circuit breaker").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[5]/td")),'Y'), 50000);
    var artikle = element.all(by.xpath("//tbody[@class='dx-template-wrapper']"))
    browser.actions().doubleClick(artikle.get(0)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    var tabheader = element.all(by.className("page-tab-header-item"));
    browser.wait(EC.elementToBeClickable(tabheader.get(2)),5000);
    browser.actions().click(tabheader.get(2)).perform();
    
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.sleep(4000);
    browser.wait(EC.invisibilityOf(element(by.xpath("//*[@class='tab-content-detail-field-text'][text()='Kleur']"))), 11000);
    expect(element(by.xpath("//*[@class='tab-content-detail-field-text'][text()='Kleur']")).isPresent).toBeTruthy();
    var inputbox = element.all(by.className("dx-texteditor-input"));
    browser.actions().doubleClick(inputbox.get(24)).sendKeys(protractor.Key.BACK_SPACE).perform();
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
  });

  it('delete the characteristics',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    element.all(by.className("page-tab-header-item")).get(1).click(); 
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.actions().doubleClick(element(by.id("grid-page-top-bar-search-input"))).sendKeys("installatieautomaat").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.sleep(1000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.sleep(1000);
    var deleteProperty = element.all(by.xpath("//em[@class='fal fa-trash-alt']"));
    deleteProperty.get(0).click();

    browser.sleep(2500);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[text()='Doorgaan']"))), 30000);
    element(by.xpath("//*[text()='Doorgaan']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    var deleteProperty = element.all(by.xpath("//em[@class='pointer fal fa-trash-alt']"));
    deleteProperty.get(8).click();
    browser.sleep(2500);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[text()='Doorgaan']"))), 30000);
    element(by.xpath("//*[text()='Doorgaan']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.xpath("//*[text='Miniature circuit breaker']"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
    
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("notification-popup-exit"))), 90000);
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });
});

describe('generated description', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
   

  it('create manufacturer by generated description',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.actions().doubleClick(element(by.id("grid-page-top-bar-search-input"))).sendKeys("installatieautomaat").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(3000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']"))), 5000);
    element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']")).click();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.wait(EC.visibilityOf(element.all(by.className("list-table")).get(5)), 5000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Item toevoegen']"))), 5000 );
    element(by.xpath("//button[text()='Item toevoegen']")).click();
    var inputbox = element.all(by.className("dx-texteditor-input"));
    browser.actions().click(inputbox.get(1)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Artikel - Fabrikant naam']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(2)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Waarde']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(3)).sendKeys("Frabrikant: ").perform();
    browser.actions().click(inputbox.get(4)).sendKeys("_").perform();
    browser.actions().click(inputbox.get(5)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Altijd zichtbaar']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']"))), 5000);
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']")).click();
    browser.sleep(1000);
    browser.actions().mouseMove(element.all(by.className("dx-list-reorder-handle")).get(6)).perform();
    browser.actions().mouseDown().perform();
    browser.actions().mouseMove({x: 0, y: -252}).perform();
    browser.actions().mouseUp().perform();
    browser.sleep(1000);
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );

    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });

  it('look at the generated ',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    element(by.className("nav-bar-search-box")).clear();
    browser.actions().doubleClick(element(by.className("nav-bar-search-box"))).sendKeys("installatieautomaat").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[5]/td")),'Y'), 50000);
    browser.actions().click(element(by.className("nav-bar-search-box"))).sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[5]/td")),'5SY66167'), 50000);
    expect(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div/div[2]/div[3]/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[2]/div/div[1]/table/tr[3]/td")).getText()).toContain("Frabrikant: Siemens Industry_");
  });

  it('delete the manufacturer',function() {
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );

    browser.actions().click(element(by.id("grid-page-top-bar-search-input"))).sendKeys("installatieautomaat").sendKeys(protractor.Key.ENTER).perform();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );


    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']"))), 5000);
    element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']")).click();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
  
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Item toevoegen']"))), 11000 );
    
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[@class='description-builder-property'][text()='Artikel - Fabrikant naam']")),'Artikel - Fabrikant naam'), 8000 );

    var deleteProperty = element.all(by.xpath("//em[@class='fal fa-trash-alt']"));
    deleteProperty.get(0).click();

    browser.sleep(1000);
    
    browser.wait(EC.visibilityOf(element(by.xpath("//button[text()='Doorgaan']"))), 8000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Doorgaan']"))), 5000 );
    element(by.xpath("//button[text()='Doorgaan']")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.sleep(1500); // moet blijven
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    browser.sleep(1500); // moet blijven
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    element(by.id("notification-bell")).click();
    browser.sleep(1500); // moet blijven
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });
});

describe('filter from article to online', function() {
  beforeEach(function() {
  });
  
  it('filter in article',function() {
    Homepagelogin();
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    
    var searchbox = element.all(by.className("filter-element-filter-search-box"));
    var searchEnter = element.all(by.className("filter-element-filter-search-icon fal fa-search"));
    var searchlabel = element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label"));
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(3000);
    browser.actions().click(searchbox.get(0)).sendKeys("installatieautomaat").sendKeys(protractor.Key.ENTER).perform();   
    browser.actions().click(searchEnter.get(0)).perform();
    browser.actions().click(element(by.xpath("//*[@class='filter-element-selection-label']"))).perform(); 
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(1500); // moet blijven
    browser.actions().click(searchbox.get(1)).sendKeys("Schneider Electric").perform();   
    browser.actions().click(searchEnter.get(1)).perform();
    browser.actions().click(searchlabel.get(0)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(1),'Domae'), 50000);
      
    browser.actions().click(searchlabel.get(13)).perform();
  
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    
    browser.actions().mouseDown(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div[1]/div[1]/articles-filters/div/div[2]/div[3]/articles-filters-features/div/div[2]/div/div[2]/ng5-slider/span[6]"))).perform();
    browser.actions().mouseMove({x: -42, y: 0}).perform();
    browser.actions().mouseUp().perform();

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );

    browser.actions().mouseDown(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/div[1]/div[1]/articles-filters/div/div[2]/div[3]/articles-filters-features/div/div[2]/div/div[2]/ng5-slider/span[5]"))).perform();
    browser.actions().mouseMove({x: 64, y: 0}).perform();
    browser.actions().mouseUp().perform();

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );

    var filter = element.all(by.className("selected-filters-name"));

    expect(filter.count()).toBe(4); 
    expect(filter.get(0).getText()).toBe('Installatieautomaat');
    expect(filter.get(1).getText()).toBe('Schneider Electric');
    expect(filter.get(2).getText()).toBe('C');
    expect(filter.get(3).getText()).toBe('20...50 A');
    expect(element(by.xpath("//*[text()='Bestelnummer: 19200']")).getText()).toContain('19200');

  });

  it('going online',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-online"))), 20000);
    element(by.id("side-bar-online")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='3606481325921']")),'3606481325921'), 50000);
    var filter = element.all(by.className("selected-filters-name"));
    browser.wait(EC.textToBePresentInElement(filter.get(3),'A'), 50000);
    expect(filter.count()).toEqual(4);
    expect(filter.get(0).getText()).toBe('Installatieautomaat');
    expect(filter.get(1).getText()).toBe('Schneider Electric');
    expect(filter.get(2).getText()).toBe('C');
    expect(filter.get(3).getText()).toBe('20...50 A');
    var deletefilter = element.all(by.className("fa fa-times selected-filters-values"));
    deletefilter.get(3).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    deletefilter.get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    var filter = element.all(by.className("selected-filters-name"));
    expect(filter.count()).toEqual(2);
    expect(filter.get(0).getText()).toBe('Installatieautomaat');
    expect(filter.get(1).getText()).toBe('C');

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(4000);
    browser.wait(EC.visibilityOf(element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(1)), 5000);
    element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(2000);
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    
    browser.actions().mouseDown(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/online-articles/div[1]/online-filters/div/div[2]/div[7]/online-filter-feature/div/div[2]/div/div[2]/ng5-slider/span[5]"))).perform();
    browser.actions().mouseMove({x: 28, y: 0}).perform();
    browser.actions().mouseUp().perform();

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );

    browser.actions().mouseDown(element(by.xpath("/html/body/app-root/authorized-layout/div[4]/online-articles/div[1]/online-filters/div/div[2]/div[7]/online-filter-feature/div/div[2]/div/div[2]/ng5-slider/span[6]"))).perform();
    browser.actions().mouseMove({x: -164, y: 0}).perform();
    browser.actions().mouseUp().perform();

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    
    var filter = element.all(by.className("selected-filters-name"));
    expect(filter.count()).toEqual(4);
    expect(filter.get(0).getText()).toBe('Installatieautomaat');
    expect(filter.get(1).getText()).toBe('Siemens AG');
    expect(filter.get(2).getText()).toBe('C');
    // expect(filter.get(3).getText()).toBe('17...25 A');
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='4001869388212']")),'4001869388212'), 50000);
    
  });
  it('import the article',function() {
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-cloud-download"))), 20000);
    var GTIN = element(by.xpath("/html/body/app-root/authorized-layout/div[4]/online-articles/div[2]/online-articles-page/div/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody[2]/tr/td/div/div[3]/table/tr[5]/td[2]")).getText();
    var artikle = element.all(by.xpath("//tbody[@class='dx-template-wrapper']"))
    artikle.get(0).click();
    element(by.className("fal fa-cloud-download")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//button[text()='Doorgaan']"))), 50000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Doorgaan']"))), 50000 );
    element(by.xpath("//button[text()='Doorgaan']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 250000 );
    element(by.id("notification-bell")).click();
    browser.sleep(1500); // moet blijven
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Download bestand"))), 50000);
    expect(element(by.buttonText("Download bestand")).isPresent()).toBe(true);
    expect(element(by.xpath("//span[text()='Aantal geaccepteerde: 1']")).getText()).toEqual('Aantal geaccepteerde: 1');
    expect(element(by.xpath("//span[text()='Aantal afwijzingen: 0']")).getText()).toEqual('Aantal afwijzingen: 0');
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform(); 
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    loginout();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    Homepagelogin();
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.urlContains('articles'), 5000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).isPresent().then(function(result) {
    if ( result ) {
      element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
    }
    });
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    browser.actions().doubleClick(element(by.className('nav-bar-search-box'))).sendKeys(GTIN).perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);
    browser.sleep(2000);   
    browser.wait(EC.visibilityOf(element(by.className("artilce-info-row"))), 80000);
    browser.wait(EC.elementToBeClickable(element(by.className("artilce-info-row"))), 20000);
    element(by.className("artilce-info-row")).click();
    browser.wait(EC.elementToBeClickable( element(by.xpath("//em[@class='fal fa-trash']"))) , 50000);
    element(by.xpath("//em[@class='fal fa-trash']")).click();  
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Doorgaan"))), 100000);
    element(by.buttonText("Doorgaan")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 80000);
    browser.sleep(4000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 80000);
    browser.sleep(4000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 80000);
    loginout();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 

  });
});

describe('Edit 5 articles', function() {
  beforeEach(function(){
    Homepagelogin();
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000); 
  });
  afterEach(function(){
    loginout();
  });
   
  
  it('select 5 articles',function() {
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).isPresent().then(function(result) {
    if ( result ) {
      element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
      browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    }
    });
    var artikle = element.all(by.className("artilce-info-row"));
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    artikle.get(0).click();
    browser.actions().keyDown(protractor.Key.CONTROL).keyDown(protractor.Key.SHIFT).perform();
    browser.sleep(2000);
    browser.actions().mouseMove(artikle.get(4)).perform();
    artikle.get(4).click();
    browser.actions().keyUp(protractor.Key.CONTROL).keyUp(protractor.Key.SHIFT).perform();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    browser.sleep(5000);
    var checkbox = element.all(by.className("dx-checkbox-icon"));
    browser.wait(EC.elementToBeClickable(checkbox.get(0)), 5000 );
    checkbox.get(0).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Technische gegevens']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Technische gegevens']")).click();
    var inputbox = element.all(by.className("dx-texteditor-input"));
    inputbox.get(7).sendKeys(10); 
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);   
    element(by.className("fal fa-save")).click();
    browser.sleep(1500); // moet sws blijven staan
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });

  it('expect preferred item',function() {

    browser.wait(EC.visibilityOf(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']"))), 9000);
    
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")),'(5)'),9000);

    expect(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")).isPresent()).toBe(true);
    expect(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")).isSelected()).toBe(false);
    element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")).click();
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    expect(element(by.className("selected-filters-name")).getText()).toEqual('Voorkeursitem');
    var artikle = element.all(by.className("article-grid-infotitle"));
    browser.actions().doubleClick(artikle.get(0)).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Technische gegevens']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Technische gegevens']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000);
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='breadcrumb-item-link'][text()='Artikelen']"))), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000);
    element(by.xpath("//*[@class='breadcrumb-item-link'][text()='Artikelen']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000);
  });
  it('delete preferred item',function() {

    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    browser.wait(EC.visibilityOf(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']"))), 9000);
    
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")),'(5)'),9000);
    expect(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")).isPresent()).toBe(true);
    element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    browser.sleep(2000);
    var artikle = element.all(by.className("article-grid-infotitle"));
    artikle.get(0).click();
    browser.actions().mouseMove(artikle.get(4)).perform();
    browser.actions().keyDown(protractor.Key.CONTROL).keyDown(protractor.Key.SHIFT).perform();
    artikle.get(4).click();
    browser.actions().keyUp(protractor.Key.CONTROL).keyUp(protractor.Key.SHIFT).perform();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    browser.sleep(3000);
    var checkbox = element.all(by.className("dx-checkbox-icon"));
    browser.wait(EC.elementToBeClickable(checkbox.get(0)), 5000 );
    checkbox.get(0).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Technische gegevens']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Technische gegevens']")).click();
    var inputbox = element.all(by.className("dx-texteditor-input"));
    browser.actions().doubleClick(inputbox.get(7)).sendKeys(protractor.Key.BACK_SPACE).sendKeys(protractor.Key.BACK_SPACE).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    element(by.className("fal fa-save")).click();
    browser.sleep(1500); // moet sws blijven staan
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']"))), 20000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    expect(element(by.xpath("//span[@class='filter-element-selection-label filter-element-selection-checkbox-label'][text()='Ja (5)']")).isPresent()).toBe(false);
  });
});

describe('export "magneetschakelaar" with characteristics', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
   
   
  it('download by "magneetschakelaar"',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    browser.sleep(2000);
      
    var searchbox = element.all(by.className("filter-element-filter-search-box"));
    var searchEnter = element.all(by.className("filter-element-filter-search-icon fal fa-search"));
    var searchlabel = element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label"))
    browser.actions().click(searchbox.get(0)).sendKeys("magneetschakelaar").perform();   
    browser.actions().click(searchEnter.get(0)).perform();
    browser.sleep(2000);
    browser.actions().click(element(by.xpath("//*[@class='filter-element-selection-label'][1]"))).perform(); 
      
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 3RF24201AC45']")),'3RF24201AC45'), 50000);
  
    var filter = element.all(by.className("selected-filters-name"));
       
    expect(filter.get(0).getText()).toBe('Magneetschakelaar');
      
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-file-download"))), 20000);
    element(by.className("fal fa-file-download")).click();
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().click(inputbox.get(0)).perform();
    browser.wait(EC.visibilityOf(element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Excel']"))), 25000 );
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Excel']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']"))), 20000);
    element(by.xpath("/html/body/app-root/authorized-layout/div[4]/articles-overview/export-articles-popup/div/dx-validation-group/div/div[2]/div[4]/dx-check-box/div/span")).click();
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Download bestand"))), 90000);
    browser.sleep(1000); // moet sws blijven staan
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();

    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']"))), 20000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
    
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
  });
});

describe('make a new mapping', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
   
   
  it('going to mapping',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']")).click();
    var wait = element.all(by.className("dx-loadindicator-content"));
    browser.wait(EC.invisibilityOf(wait.get(0)), 6000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-plus"))),5000);
    element(by.className("fal fa-plus")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[@class='modal-bottom-button'][text()='Voeg toe ']"))), 8000 );
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().click(inputbox.get(1)).sendKeys("TMexcel").perform();
    browser.actions().click(inputbox.get(2)).sendKeys("TMexcel").perform();
    browser.actions().click(inputbox.get(3)).perform();
    browser.wait(EC.visibilityOf(element(by.className("dx-overlay-wrapper dx-dropdowneditor-overlay dx-popup-wrapper dx-dropdownlist-popup-wrapper dx-selectbox-popup-wrapper"))), 5000);
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Excel']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-dropdowneditor-overlay dx-popup-wrapper dx-dropdownlist-popup-wrapper dx-selectbox-popup-wrapper"))), 5000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[@class='modal-bottom-button'][text()='Voeg toe ']"))), 5000 );
    element(by.xpath("//button[@class='modal-bottom-button'][text()='Voeg toe ']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(inputbox.get(1)), 5000 );
    inputbox.get(1).sendKeys("Article number");
    inputbox.get(2).sendKeys("Artikel ID");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"Artikel ID"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    inputbox.get(3).sendKeys("Type number");
    inputbox.get(4).sendKeys("Typenummer");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"Typenummer"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    inputbox.get(5).sendKeys("Description1");
    inputbox.get(6).sendKeys("Gegenereerde omschrijving");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"Gegenereerde omschrijving"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    inputbox.get(7).sendKeys("Manufacturer code");
    inputbox.get(8).sendKeys("Fabrikant code");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"Fabrikant code"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    inputbox.get(9).sendKeys("Remark1");
    inputbox.get(10).sendKeys("Notities");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"Notities"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    inputbox.get(11).sendKeys("Order number");
    inputbox.get(12).sendKeys("Bestelnummer");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"Bestelnummer"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    inputbox.get(13).sendKeys("EAN code");
    inputbox.get(14).sendKeys("GTIN");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"GTIN"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();

    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
  });

  it('EPLAN mapping change',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']")).click();
    browser.wait(EC.elementToBeClickable(element.all(by.className("dx-dropdowneditor-icon")).get(0)), 10000 );
    element.all(by.className("dx-dropdowneditor-icon")).get(0).click();
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='EPLAN']")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().doubleClick(inputbox.get(8)).sendKeys(protractor.Key.BACK_SPACE).perform();
    inputbox.get(8).sendKeys("GTIN");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"GTIN"), 5000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
  });
  
  it('Expect downoad button by Excel mapping',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(3000)
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).isPresent().then(function(result) {
      if ( result ) {
        element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
      }
      });
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);

    browser.sleep(1500);
    var searchbox = element.all(by.className("filter-element-filter-search-box"));
    var searchEnter = element.all(by.className("filter-element-filter-search-icon fal fa-search"));
    var searchlabel = element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label"))
    browser.actions().click(searchbox.get(0)).sendKeys("magneetschakelaar").perform();   
    browser.actions().click(searchEnter.get(0)).perform();
    browser.sleep(2000);
    browser.actions().click(element(by.xpath("//*[@class='filter-element-selection-label'][1]"))).perform();
      
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 3RF24201AC45']")),'3RF24201AC45'), 50000);

    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-file-download"))), 20000);
    element(by.className("fal fa-file-download")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']"))), 20000);
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().click(inputbox.get(0)).perform();
    browser.wait(EC.visibilityOf(element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Excel Mapping']"))), 25000 );
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Excel Mapping']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']"))), 20000);

    browser.actions().click(inputbox.get(1)).perform();
    browser.wait(EC.visibilityOf(element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='TMexcel']"))), 25000 );
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='TMexcel']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']"))), 20000);
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );

    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Download bestand"))), 900000);
    browser.sleep(1000); // moet sws blijven staan
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
   
  });

  it('Expect downoad button by Eplan',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(3000);
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).isPresent().then(function(result) {
      if ( result ) {
        element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
      }
      });
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);

    browser.sleep(1500);

    var searchbox = element.all(by.className("filter-element-filter-search-box"));
    var searchEnter = element.all(by.className("filter-element-filter-search-icon fal fa-search"));
    var searchlabel = element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label"))
    browser.actions().click(searchbox.get(0)).sendKeys("magneetschakelaar").perform();   
    browser.actions().click(searchEnter.get(0)).perform();
    browser.sleep(2000);
    browser.actions().click(element(by.xpath("//*[@class='filter-element-selection-label'][1]"))).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 3RF24201AC45']")),'3RF24201AC45'), 50000);

    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-file-download"))), 20000);
    element(by.className("fal fa-file-download")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']"))), 20000);
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().click(inputbox.get(0)).perform();
    browser.wait(EC.visibilityOf(element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Eplan']"))), 250000 );
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='Eplan']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']"))), 200000);

    browser.actions().click(inputbox.get(1)).perform();
    browser.wait(EC.visibilityOf(element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='EPLAN']"))), 250000 );
    var Eplansingle = element.all(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='EPLAN']"));
    expect(Eplansingle.count()).toBe(1);
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='EPLAN']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']"))), 200000);
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Download ']")).click();

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 400000);
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Download bestand"))), 900000);
    browser.sleep(1000); // moet sws blijven staan
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
    browser.sleep(3000)
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).isPresent().then(function(result) {
      if ( result ) {
        element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
      }
      });

  });

  it('Delete the TMexcel',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(4000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']"))), 500000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']")).click();
    var wait = element.all(by.className("dx-loadindicator-content"));
    browser.wait(EC.invisibilityOf(wait.get(0)), 800000);
    browser.wait(EC.elementToBeClickable(element.all(by.className("dx-dropdowneditor-icon")).get(0)), 10000 );
    element.all(by.className("dx-dropdowneditor-icon")).get(0).click();
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='TMexcel']")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash"))), 50000 );
    element(by.className("fal fa-trash")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Doorgaan"))), 50000 );
    element(by.buttonText("Doorgaan")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
  });

  it('EPLAN mapping change',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),80000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']"))), 50000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Mappings']")).click();
    var wait = element.all(by.className("dx-loadindicator-content"));
    browser.wait(EC.invisibilityOf(wait.get(0)), 800000);
    browser.wait(EC.elementToBeClickable(element.all(by.className("dx-dropdowneditor-icon")).get(0)), 10000 );
    element.all(by.className("dx-dropdowneditor-icon")).get(0).click();
    element(by.xpath("//div[@class='dx-item-content dx-list-item-content'][text()='EPLAN']")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 50000 );
    element(by.className("fal fa-pencil")).click();
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().doubleClick(inputbox.get(8)).sendKeys(protractor.Key.BACK_SPACE).perform();
    inputbox.get(8).sendKeys("Groepnaam");
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")),"Groepnaam"), 50000);
    element(by.xpath("/html/body/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div/div")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 50000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 400000);
  });
});

describe('functions with online on the online page', function() {
  beforeEach(function() {
    Homepagelogin();
    element(by.id("side-bar-online")).click();
    browser.wait(EC.urlContains('online/search'), 5000);
  });
  afterEach(function(){
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    loginout();
  });
   

  it('search in online on PKZ motorbeveiliging',function() {
    element(by.className('online-search-input-box')).sendKeys("PKZ motorbeveiliging");
    element(by.buttonText('Zoeken...')).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).isPresent().then(function(result) {
      if ( result ) {
        element(by.xpath("//*[@class='fal fa-trash'][@style='line-height: 0.6']")).click();
        browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
        browser.actions().doubleClick(element(by.className('nav-bar-search-box'))).sendKeys("PKZ motorbeveiliging").perform();
        browser.actions().sendKeys(protractor.Key.ENTER).perform();
        browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
      }
      });

    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='4015080727200']")),'4015080727200'), 60000);
  
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element.all(by.className("filter-element-selection-label")).get(0),'Motorbeveiligingsschakelaar'), 10000);
    element.all(by.className("filter-element-selection-label")).get(0).click();
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(0),'Eaton'), 10000);
    element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(0).click();
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(0),'Thermomagnetisch'), 10000);
    element.all(by.className("filter-element-selection-label filter-element-selection-checkbox-label")).get(0).click();
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.sleep(3000);
    browser.actions().doubleClick(element(by.xpath("//*[@class='filter-element-range-box-right'][@id='numericEndEF001389']"))).sendKeys("32").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );

    browser.sleep(2000);
    var filter = element.all(by.className("selected-filters-name"));
    expect(filter.count()).toBe(4);
    expect(filter.get(0).getText()).toBe('Motorbeveiligingsschakelaar');
    expect(filter.get(1).getText()).toBe('Eaton');
    expect(filter.get(2).getText()).toBe('Thermomagnetisch');
    expect(filter.get(3).getText()).toBe('0.16...32 A');
    expect(element(by.xpath("//*[text()='4015082860851']")).getText()).toBe('4015082860851');
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
  
  });

  it('bulk inport',function() {
  
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-loadpanel-message"))), 80000 );

    element(by.id("notification-bell")).click();
    browser.sleep(1500);
    element(by.className("fal fa-trash pointer")).isPresent().then(function(result) {
    if ( result ) {
      element(by.className("fal fa-trash pointer")).click();
    }
    });
    browser.actions().click(element(by.className("notification-popup-exit"))).perform(); 
    element(by.buttonText('Bulk import')).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[@class='modal-title'][text()='Importeer artikelen']")),'Importeer artikelen'), 5000);
    element(by.className("dx-texteditor-input")).sendKeys("3606480082153 3606480082191 360640821 36064800813");
    browser.wait(EC.elementToBeClickable(element.all(by.className("modal-bottom-button")).get(0)), 2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-loadpanel-message"))), 50000 );
    element.all(by.className("modal-bottom-button")).get(0).click();
    browser.sleep(1500); 
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    browser.sleep(1500); 
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    element(by.id("notification-bell")).click();
    browser.sleep(1500); 
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Download bestand"))), 50000);
    expect(element(by.buttonText("Download bestand")).isPresent()).toBe(true);
    expect(element(by.xpath("//span[text()='Aantal geaccepteerde: 2']")).getText()).toEqual('Aantal geaccepteerde: 2');
    expect(element(by.xpath("//span[text()='Aantal afwijzingen: 2']")).getText()).toEqual('Aantal afwijzingen: 2');
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });

});

describe('delete the created articles', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
   


  it('3606480082153',function() {

    browser.actions().doubleClick(element(by.className('nav-bar-search-box'))).sendKeys("3606480082153").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: A9F83170']")),'A9F83170'), 80000);
    browser.sleep(1000);
    element(by.className("artilce-info-row")).click();
    browser.wait(EC.elementToBeClickable( element(by.xpath("//em[@class='fal fa-trash']"))) , 5000);
    element(by.xpath("//em[@class='fal fa-trash']")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Doorgaan"))), 10000);
    element(by.buttonText("Doorgaan")).click();
    browser.sleep(2000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 80000);
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 80000);
    browser.sleep(1500);

  });

  it('3606480082191',function() {
    
    browser.actions().doubleClick(element(by.className('nav-bar-search-box'))).sendKeys("3606480082191").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: A9F83370']")),'A9F83370'), 80000);
    browser.sleep(2000);
    element(by.className("artilce-info-row")).click();
    browser.wait(EC.elementToBeClickable( element(by.xpath("//em[@class='fal fa-trash']"))) , 5000);
    element(by.xpath("//em[@class='fal fa-trash']")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Doorgaan"))), 10000);
    element(by.buttonText("Doorgaan")).click();
    browser.sleep(2000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 70000);
    browser.sleep(1500);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 60000); 
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 50000);
    browser.sleep(2000);  
  });
}); 

describe('enrich article', function() {
  beforeEach(function() {
    Homepagelogin();
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
  });

  afterEach(function(){
    loginout();
  });
   

  it('make a new article',function() {
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 25000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    element(by.className("fal fa-plus")).click();
    browser.sleep(5000); // moet sws blijven staan
    var inputbox = element.all(by.xpath("//input[@class='dx-texteditor-input']"));
    browser.actions().click(inputbox.get(3)).sendKeys("3606480082238").perform();
    browser.actions().click(inputbox.get(0)).sendKeys("schneider").perform();
    browser.sleep(1000); // moet sws blijven staan

    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.actions().click(inputbox.get(1)).sendKeys("SEIA004").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    element(by.className("fal fa-save")).click();
    browser.sleep(1500); // moet sws blijven staan
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 250000 );
  });

  it('enrich article done',function() {
    
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );
    browser.wait(EC.elementToBeClickable(element(by.className("nav-bar-search-box"))) , 5000);
    element(by.className("nav-bar-search-box")).sendKeys("3606480082238");
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 900000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Typenummer: SEIA004']")),'SEIA004'), 30000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-sync-alt"))), 20000);
    element(by.className("fal fa-sync-alt")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Ok ']"))), 20000);
    browser.sleep(5000);
    element(by.xpath("//*[@class='dx-radiobutton-icon'][1]")).click();
    browser.sleep(1000);// moet sws blijven staan
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Ok ']")).click();
    browser.sleep(1000);// moet sws blijven staan
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Download bestand"))), 90000);
    browser.sleep(1000); // moet sws blijven staan
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 40000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: A9F83201']")),'A9F83201'), 150000);
  });

  it('delete the enrich article',function() {
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);
    browser.wait(EC.elementToBeClickable(element(by.className('nav-bar-search-box'))), 8000);
    browser.actions().doubleClick(element(by.className('nav-bar-search-box'))).sendKeys("3606480082238").perform();
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Typenummer: SEIA004']")),'SEIA004'), 100000);
    browser.sleep(1000);
    element(by.className("artilce-info-row")).click();
    browser.wait(EC.elementToBeClickable( element(by.xpath("//em[@class='fal fa-trash']"))) , 5000);
    element(by.xpath("//em[@class='fal fa-trash']")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Doorgaan"))), 10000);
    element(by.buttonText("Doorgaan")).click();
    browser.sleep(2000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 70000);
    browser.sleep(1500);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000); 
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Er zijn geen artikelen gevonden.']")),"geen artikelen gevonden"), 80000);
    browser.sleep(2000);
  });
});  

describe('grid change', function() {
  beforeEach(function() {
     Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
  it('set own field add colum',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Zelfgedefinieerde velden']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Zelfgedefinieerde velden']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element.all(by.xpath("//div[@class='custom-field-name'][text()='EPLAN Producthoofdgroep']")).get(0)), 5000 );
    element.all(by.xpath("//div[@class='custom-field-name'][text()='EPLAN Producthoofdgroep']")).get(0).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    element.all(by.className("dx-checkbox-icon")).get(0).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
  });


  it('add colum',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-th"))), 20000);
    element(by.className("fal fa-th")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-list"))), 20000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[@class='dx-datagrid-text-content dx-text-content-alignment-left'][text()='Artikel ID']")), 'Artikel ID'), 20000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-columns"))), 20000);
    element(by.className("fal fa-columns")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.visibilityOf(element(by.xpath("//div[text()='Kolomkiezer']"))), 20000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1000);
    browser.actions().mouseMove(element(by.xpath("//span[text()='EPLAN Producthoofdgroep']"))).perform();
    browser.wait(EC.elementToBeClickable(element(by.xpath("/html/body/div[3]/div/div[2]/div/div/div/div[1]/ul/li[20]/div[1]/div"))), 5000);
    element(by.xpath("/html/body/div[3]/div/div[2]/div/div/div/div[1]/ul/li[20]/div[1]/div")).click();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("/html/body/div[3]/div/div[2]/div/div/div/div[1]/ul/li[20]/div[1]/div"))), 5000);
    element(by.xpath("/html/body/div[3]/div/div[2]/div/div/div/div[1]/ul/li[20]/div[1]/div")).click();
    browser.sleep(2000);
    element(by.className("dx-icon dx-icon-close")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[@class='dx-datagrid-text-content dx-text-content-alignment-left'][text()='EPLAN Producthoofdgroep']")), 'EPLAN Producthoofdgroep'), 20000);
  });
  
  it('delete own field in colum',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Zelfgedefinieerde velden']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Zelfgedefinieerde velden']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element.all(by.xpath("//div[@class='custom-field-name'][text()='EPLAN Producthoofdgroep']")).get(0)), 5000 );
    element.all(by.xpath("//div[@class='custom-field-name'][text()='EPLAN Producthoofdgroep']")).get(0).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.sleep(2000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    element.all(by.className("dx-checkbox-icon")).get(0).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
  });
  it('change grid back',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[@class='dx-datagrid-text-content dx-text-content-alignment-left'][text()='Artikel ID']")), 'Artikel ID'), 20000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-list"))), 20000);
    element(by.className("fal fa-list")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-th"))), 20000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Bestelnummer: 62091070']")),'62091070'), 50000);
  });
  
});

describe('generated description new', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
   
  it('create manufacturer by generated description',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.actions().doubleClick(element(by.id("grid-page-top-bar-search-input"))).sendKeys("Waterpas").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(3000);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']"))), 5000);
    element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']")).click();
    browser.sleep(1000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Item toevoegen']"))), 5000 );
    element(by.xpath("//button[text()='Item toevoegen']")).click();
    var inputbox = element.all(by.className("dx-texteditor-input"));
    browser.actions().click(inputbox.get(1)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Groep - Naam']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(2)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Waarde']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(5)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Altijd zichtbaar']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']"))), 5000);
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']")).click();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Item toevoegen']"))), 5000 );
    element(by.xpath("//button[text()='Item toevoegen']")).click();
    var inputbox = element.all(by.className("dx-texteditor-input"));
    browser.actions().click(inputbox.get(1)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Artikel - GTIN']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(2)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Waarde']")).click();
    browser.sleep(1000);
    browser.actions().click(inputbox.get(3)).sendKeys(" ").perform();
    browser.actions().click(inputbox.get(5)).perform();
    element(by.xpath("//*[@class='dx-item-content dx-list-item-content'][text()='Altijd zichtbaar']")).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']"))), 5000);
    element(by.xpath("//*[@class='modal-bottom-button'][text()='Toevoegen']")).click();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    element(by.className("fal fa-save")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay dx-widget dx-visibility-change-handler dx-toast"))), 25000 );

    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    element(by.id("notification-bell")).click();
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });

  it('look at the generated ',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-articles"))), 20000);
    element(by.id("side-bar-articles")).click();
    browser.wait(EC.urlContains('articles'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    element(by.className("nav-bar-search-box")).clear();
    browser.actions().doubleClick(element(by.className("nav-bar-search-box"))).sendKeys("Waterpas").sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Typenummer: 660050111']")),'660050111'), 50000);
    browser.actions().click(element(by.className("nav-bar-search-box"))).sendKeys(protractor.Key.ENTER).perform();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 80000 );
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Typenummer: 660050111']")),'660050111'), 50000);
    expect(element(by.xpath("//*[text()='waterpas 4012078587327']")).getText()).toContain("waterpas 4012078587327");
  });

  it('delete the manufacturer',function() {
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.urlContains('manufacturers'), 5000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1000);
    element.all(by.className("page-tab-header-item")).get(1).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );

    browser.actions().click(element(by.id("grid-page-top-bar-search-input"))).sendKeys("Waterpas").sendKeys(protractor.Key.ENTER).perform();
    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(1500);
    var groeplines = element.all(by.className("dx-row dx-data-row dx-column-lines"));
    browser.actions().doubleClick(groeplines.get(0)).perform();
    browser.sleep(1000);

    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    
    browser.wait(EC.elementToBeClickable(element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']"))), 5000);
    element(by.xpath("//*[@class='page-tab-header-item-text'][text()='Gegenereerde omschrijving']")).click();

    browser.sleep(1000);
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-pencil"))), 5000 );
    element(by.className("fal fa-pencil")).click();
  
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Item toevoegen']"))), 11000 );
    browser.sleep(9000);
    var deleteProperty = element.all(by.xpath("//em[@class='fal fa-trash-alt']"));
    deleteProperty.get(1).click();
    browser.sleep(1000);
    browser.wait(EC.visibilityOf(element(by.xpath("//button[text()='Doorgaan']"))), 800000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Doorgaan']"))), 5000 );
    element(by.xpath("//button[text()='Doorgaan']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    deleteProperty.get(0).click();
    browser.sleep(1000);
    browser.wait(EC.visibilityOf(element(by.xpath("//button[text()='Doorgaan']"))), 800000 );
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[text()='Doorgaan']"))), 5000 );
    element(by.xpath("//button[text()='Doorgaan']")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );

    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-save"))), 5000 );
    element(by.className("fal fa-save")).click();
    browser.sleep(1500); // moet blijven
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    browser.wait(EC.elementToBeClickable(element(by.id("notification-bell"))), 50000);
    browser.sleep(1500); // moet blijven
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 50000 );
    element(by.id("notification-bell")).click();
    browser.sleep(1500); // moet blijven
    browser.wait(EC.visibilityOf(element(by.xpath("//*[@class='background-job-title-text'][text()='Update dynamische artikelwaarden']"))), 5000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("/html/body/app-root/authorized-layout/background-notification-popup/div[2]/div/div[2]/div[1]/div[2]")),"Taak is voltooid"),300000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash pointer"))), 90000);
    element(by.className("fal fa-trash pointer")).click();
    browser.actions().click(element(by.className("notification-popup-exit"))).perform();
  });
});

describe('search for  new supplier', function() {
  beforeEach(function() {
    Homepagelogin();
  });
  afterEach(function(){
    loginout();
  });
  it('search new supplier',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Leveranciers']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Leveranciers']")).click();
    browser.wait(EC.textToBePresentInElement(element(by.className("dx-datagrid-nodata")), 'No data') ,3000);
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-plus"))),5000);
    element(by.className("fal fa-plus")).click();
    browser.wait(EC.elementToBeClickable(element(by.className(" online-search-input-box"))),5000);
    element(by.className(" online-search-input-box")).sendKeys("Technische Unie ALKMAAR");
    element(by.className("online-search-input-icon fal fa-search")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Technische Unie ALKMAAR']")), 'Technische Unie ALKMAAR') ,3000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='2220000095631']")), '2220000095631') ,3000);
    element.all(by.className("dx-checkbox-icon")).get(1).click();
    browser.wait(EC.elementToBeClickable(element(by.xpath("//button[@class='modal-bottom-button'][text()='Toevoegen ']"))), 5000 );
    element(by.xpath("//button[@class='modal-bottom-button'][text()='Toevoegen ']")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Technische Unie ALKMAAR']")), 'Technische Unie ALKMAAR') ,3000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='2220000095631']")), '2220000095631') ,3000);

  });
  it('check the new supplier',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Leveranciers']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Leveranciers']")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Technische Unie ALKMAAR']")), 'Technische Unie ALKMAAR') ,3000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='2220000095631']")), '2220000095631') ,3000);
    expect(element(by.xpath("//img[@class='supplier-source-logo'][@alt='2BALogo']")).isPresent()).toBe(true); 

  });

  it('delete the supplier',function() {
    browser.wait(EC.elementToBeClickable(element(by.id("side-bar-management"))),8000);
    element(by.id("side-bar-management")).click();
    browser.wait(EC.invisibilityOf(element(by.className("dx-overlay-wrapper dx-loadpanel-wrapper dx-overlay-modal dx-overlay-shader"))), 70000 );
    browser.sleep(2000);
    browser.wait(EC.elementToBeClickable(element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Leveranciers']"))), 5000 );
    element(by.xpath("//div[@class='page-tab-header-item-text'][text()='Leveranciers']")).click();
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='Technische Unie ALKMAAR']")), 'Technische Unie ALKMAAR') ,3000);
    browser.wait(EC.textToBePresentInElement(element(by.xpath("//*[text()='2220000095631']")), '2220000095631') ,3000);
    element(by.className("dx-row dx-data-row dx-column-lines")).click();
    browser.wait(EC.elementToBeClickable(element(by.className("fal fa-trash"))), 50000);
    element(by.className("fal fa-trash")).click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("Doorgaan"))), 5000 );
    element(by.buttonText("Doorgaan")).click();
    browser.wait(EC.textToBePresentInElement(element(by.className("dx-datagrid-nodata")), 'No data') ,3000);
  });

});
